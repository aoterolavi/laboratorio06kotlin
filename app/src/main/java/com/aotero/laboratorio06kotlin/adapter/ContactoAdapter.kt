package com.aotero.laboratorio06kotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.aotero.laboratorio06kotlin.R
import com.aotero.laboratorio06kotlin.model.Contacto
import kotlinx.android.synthetic.main.item_contactos.view.*

//1.Recibo la informacion
//3. Metodos del adaptador
class ContactoAdapter( var contactos: MutableList<Contacto>) : RecyclerView.Adapter<ContactoAdapter.ContactoAdapterViewHolder>() {

    //2.Creamos subclase(clase interna) llamada viewholder
    //itemView = item_contactos.xml
    inner class ContactoAdapterViewHolder(itemView:View): RecyclerView.ViewHolder(itemView){

        //4
        fun bind(contacto: Contacto){
            //Mostrar los valores en el xml item_contactos.xml
            itemView.tvNombre.text=contacto.nombre
            itemView.tvCargo.text=contacto.cargo
            itemView.tvCorreo.text=contacto.correo
            itemView.tvLetra.text=contacto.letra
        }
    }

    //3.2
    //Presenta al adapter la vista con la cual va a trabajar
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactoAdapterViewHolder {

        //view =item_contactos.xml
        val view =LayoutInflater.from(parent.context).inflate(R.layout.item_contactos,parent,false)
        return ContactoAdapterViewHolder(view)
    }

    //3.3
    //itera tantas veces como tenga mi lista
    override fun onBindViewHolder(holder: ContactoAdapterViewHolder, position: Int) {
        val contacto   = contactos[position]


        holder.bind(contacto)

    }

    //3.1
    //Cantidad de elementos tiene mi lista
    override fun getItemCount(): Int {
        return contactos.size
    }
}