package com.aotero.laboratorio06kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.aotero.laboratorio06kotlin.adapter.ContactoAdapter
import com.aotero.laboratorio06kotlin.databinding.ActivityMainBinding
import com.aotero.laboratorio06kotlin.model.Contacto

class MainActivity : AppCompatActivity() {

    private lateinit var binding:ActivityMainBinding
    private var contactos = mutableListOf<Contacto>()

    private lateinit var adaptador :ContactoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
         binding = ActivityMainBinding.inflate(layoutInflater)
         setContentView(binding.root)


        loadData()
        setupAdapter()
    }

    private fun setupAdapter() {

        adaptador = ContactoAdapter(contactos)
        binding.rvListaContactos.adapter =adaptador
        binding.rvListaContactos.layoutManager=LinearLayoutManager(this)
        //binding.rvListaContactos.layoutManager=GridLayoutManager(this,4)
    }

    private fun loadData() {

        contactos.add(Contacto("Aaron Otero","Kotlin Developer","aaronoterolavi@gmail.com","A"))
        contactos.add(Contacto("Tania Pastor","Front End Developer","ThaniaPastor@gmail.com","T"))
        contactos.add(Contacto("Andres Otero","CEO","andyTero@gmail.com","A"))
        contactos.add(Contacto("Flor de Maria Lavi Sojos","Gerente General","florlavi@gmail.com","F"))
        contactos.add(Contacto("Ten Shin","Jefe de Tecnologia","tenshin@gmail.com","T"))
        contactos.add(Contacto("Aaron Otero","Kotlin Developer","aaronoterolavi@gmail.com","A"))
        contactos.add(Contacto("Tania Pastor","Front End Developer","ThaniaPastor@gmail.com","T"))
        contactos.add(Contacto("Andres Otero","CEO","andyTero@gmail.com","A"))
        contactos.add(Contacto("Flor de Maria Lavi Sojos","Gerente General","florlavi@gmail.com","F"))
        contactos.add(Contacto("Ten Shin","Jefe de Tecnologia","tenshin@gmail.com","T"))
    }
}