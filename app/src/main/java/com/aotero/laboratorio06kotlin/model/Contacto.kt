package com.aotero.laboratorio06kotlin.model

data class Contacto(var nombre:String
                    ,var cargo:String
                    ,var correo:String
                    ,var letra:String) {
}